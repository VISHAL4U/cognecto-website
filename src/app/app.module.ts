import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home/home.component';
import {AngularFullpageModule} from '@fullpage/angular-fullpage';
import {MalihuScrollbarModule} from 'ngx-malihu-scrollbar';
import {FullpageComponent} from './home/fullpage/fullpage.component';
import { NavbarComponent } from './navbar/navbar.component';
import { Home2Component } from './home/home2/home2.component';
import { Home3Component } from './home/home3/home3.component';
import { Home4Component } from './home/home4/home4.component';
import { Home5Component } from './home/home5/home5.component';
import { Home6Component } from './home/home6/home6.component';
import { Home7Component } from './home/home7/home7.component';
import { Home8Component } from './home/home8/home8.component';
import { Home9Component } from './home/home9/home9.component';
import { Home4part2Component } from './home/home4part2/home4part2.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FullpageComponent,
    NavbarComponent,
    Home2Component,
    Home3Component,
    Home4Component,
    Home5Component,
    Home6Component,
    Home7Component,
    Home8Component,
    Home9Component,
    Home4part2Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFullpageModule, // added
    MalihuScrollbarModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
