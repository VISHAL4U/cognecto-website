import {AfterViewInit, Component, OnInit} from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-home7',
  templateUrl: './home7.component.html',
  styleUrls: ['./home7.component.scss']
})
export class Home7Component implements OnInit, AfterViewInit {

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      $('.pageWrapper').addClass('animate__backInUp');
    }, 1500);

  }
}
