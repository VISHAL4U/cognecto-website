import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MalihuScrollbarService} from 'ngx-malihu-scrollbar';

export * from 'gsap';

declare var $: any;
declare var fullpage_api: any;


@Component({
  selector: 'app-fullpage',
  templateUrl: './fullpage.component.html',
  styleUrls: ['./fullpage.component.scss']
})
export class FullpageComponent implements OnInit, AfterViewInit, OnDestroy {
  config: any;

  @ViewChild('sectionRef', {static: false})
  sectionRef: ElementRef;

  constructor(private elemRef: ElementRef, private mScrollbarService: MalihuScrollbarService) {
    const that = this;
    this.config = {
      licenseKey: '5BFFB061-C6B5493B-AFD16F4D-D3AADF20',
      lockAnchors: true,
      navigationTooltips: [],
      css3: true,
      navigation: true,
      navigationPosition: 'right',
      showActiveTooltip: false,
      slidesNavigation: false,
      slidesNavPosition: 'bottom',
      scrollingSpeed: 700,
      autoScrolling: true,
      fitToSection: true,
      fitToSectionDelay: 1000,
      scrollBar: false,
      easing: 'easeInOutCubic',
      easingcss3: 'ease',
      loopBottom: true,
      loopTop: false,
      loopHorizontal: true,
      continuousVertical: false,
      continuousHorizontal: false,
      scrollHorizontally: true,
      interlockedSlides: false,
      dragAndMove: false,
      offsetSections: false,
      resetSliders: true,
      fadingEffect: '.section',
      scrollOverflow: false,
      scrollOverflowReset: false,
      scrollOverflowOptions: null,
      touchSensitivity: 15,
      bigSectionsDestination: null,
      sectionsColor: ['#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff'],

      // Accessibility
      keyboardScrolling: true,
      animateAnchor: true,
      recordHistory: true,

      // Design
      controlArrows: false,
      verticalCentered: true,
      fixedElements: '#header, .footer',
      responsiveWidth: 760,
      responsiveHeight: 0,
      responsiveSlides: false,
      parallax: true,
      dropEffect: true,
      parallaxOptions: {type: 'reveal', percentage: 62, property: 'background'},
      dropEffectOptions: {speed: 2300, color: '#F82F4D', zIndex: 9999},
      waterEffectOptions: {animateContent: true, animateOnMouseMove: true},
      cardsOptions: {perspective: 100, fadeContent: true, fadeBackground: true},
      waterEffect: true,
      cards: true,
      sectionSelector: '.section',
      slideSelector: '.slide',
      lazyLoading: true,


      onLeave(origin, destination, direction) {
        that.leave(origin, destination, destination);
        if (destination.isLast) {
          $('#section10').hide();
        } else {
          $('#section10').show();
        }
        console.log('leave>>>', 'origin', origin, 'destination', destination, 'destination', destination);
      },
      afterLoad(origin, destination, direction) {
        setTimeout(() => {
          $('.loaderBg').hide();
        }, 500);

        that.afterLoad(origin, destination, direction);
        console.log('afterLoad>>>', 'origin', origin, 'destination', destination, 'destination', destination);
      },
      afterRender() {
        that.afterRender();
        setTimeout(() => {
          $('.loaderBg').hide();
        }, 500);
      },
      afterSlideLoad(section, origin, destination, direction) {
        $('.loaderBg').hide();
        that.afterSlideLoad(section, origin, destination, direction);
        // tslint:disable-next-line:max-line-length
        console.log('afterSlideLoad>>>', 'section', section, 'origin', origin, 'destination', destination, 'direction', direction);
      },
      onSlideLeave(section, origin, destination, direction) {
        that.onSlideLeave(section, origin, destination, direction);
        console.log('onSlideLeave>>>', 'section', section, 'origin', origin, 'destination', destination, 'direction', direction);
      }
    };
  }

  ngOnInit() {

  }

  getRef(e: any) {

  }

  leave(origin, destination, direction) {

  }

  afterLoad(origin, destination, direction) {
    const tl = new TimelineMax();

    if (destination.index === 0) {
      const upImge = document.querySelectorAll('.upperImg');
      const middleImge = document.querySelectorAll('.middleImg');
      const lastImge = document.querySelectorAll('.lowerImg');
      const selct = document.querySelectorAll('.selector');
      const subtext = document.querySelectorAll('.subtext');
      const orange = document.querySelectorAll('.orange');
      const headerLastText = document.querySelectorAll('.headerLastText');

      tl.fromTo(upImge, 0.4, {y: '-100%'}, {y: '-0%'});
      tl.fromTo(middleImge, 0.5, {x: '200%'}, {x: '-0%'});
      tl.fromTo(lastImge, 0.6, {y: '100%'}, {y: '-0%'});
      tl.fromTo(selct, 0.9, {y: '-200%'}, {y: '0%'});
      tl.fromTo(subtext, 0.9, {x: '-200%'}, {x: '0%'});
      tl.fromTo(orange, 0.9, {y: '1000%'}, {y: '0%'});
      tl.fromTo(headerLastText, 0.9, {y: '1000%'}, {y: '-0%'});
    }

    if (destination.index === 2) {
      const leftimage = document.querySelectorAll('.left-container');
      const selector = document.querySelectorAll('.selector');
      const subtext = document.querySelectorAll('.subtext');
      const bodytext = document.querySelectorAll('.bodytext');
      const items = document.querySelectorAll('.counter');

      tl.fromTo(leftimage, 0.5, {y: '-100%'}, {y: '-0%'});
      tl.fromTo(selector, 0.7, {y: '50', opacity: 0}, {y: '0', opacity: 1});
      tl.fromTo(subtext, 0.8, {x: '200%'}, {x: '-0%'});
      tl.fromTo(bodytext, 0.9, {y: '50', opacity: 0}, {y: '0', opacity: 1});
      gsap.from(items[0], {
        textContent: 0,
        duration: 8,
        ease: 'power1.in',
        snap: {textContent: 1},
        stagger: {
          each: 1.0,
          onUpdate: function () {
            this.targets()[0].innerHTML = this.numberWithCommas(Math.ceil(this.targets()[0].textContent));
          },
        }
      });
      setTimeout(() => {
        gsap.from(items[1], {
          textContent: 0,
          duration: 10,
          ease: 'power1.in',
          snap: {textContent: 1},
          stagger: {
            each: 1.0,
            onUpdate: function () {
              this.targets()[0].innerHTML = this.numberWithCommas(Math.ceil(this.targets()[0].textContent));
            },
          }
        });
      }, 500);


    }
  }

  numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '');
  }

  afterRender() {

  }

  afterSlideLoad(section, origin, destination, direction) {
    // this.leave(origin, destination, direction);
    $('.loaderBg').hide();
  }

  onSlideLeave(section, origin, destination, direction) {

  }

  ngAfterViewInit() {
    fullpage_api('#fullpage', this.config);
    setTimeout(() => {
      $('.loaderBg').hide();
    }, 500);
  }

  ngOnDestroy() {

  }
}
