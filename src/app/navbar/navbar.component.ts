import {Component, OnInit} from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isActive = false;

  constructor() {
  }

  ngOnInit() {
  }

  openMenu(e: any): void {
    this.isActive = !this.isActive;
    // @ts-ignore
    this.isActive ? $('.menuList').toggleClass('slideLeft') : $('.menuList').toggleClass('slideRight');
    // @ts-ignore
    $('.menu').attr('aria-expanded', $('.menu').hasClass('.opened'));
  }
}
