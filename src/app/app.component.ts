import {Component} from '@angular/core';

declare var fullpage_api: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'COGNECTO';

  nextSlide() {
    fullpage_api.moveSectionDown();
  }
}
